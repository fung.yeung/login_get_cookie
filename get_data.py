# -*- coding:utf-8 -*-
import requests
import os
import re
import datetime
import time

from cookie_getter import get_cookie

# from datetime import datetime

"""

Request URL: https://hubgrade2.yansanveolia.com/Configuration/DataExport/Export?date=2021-04-20&Country=test&Project=test&Site=test&site_type=test

Request URL: https://hubgrade2.yansanveolia.com/Configuration/DataExport/Export?date=2021-04-20&Country=1&Project=1&Site=1&site_type=1
Request URL: https://hubgrade2.yansanveolia.com/Configuration/DataExport/Export?types=2&date=2021-04&Country=2&Project=2&Site=2&site_type=2&confirm_interval=999
Request URL: https://hubgrade2.yansanveolia.com/Configuration/DataExport/Export?types=2&date=2021-02&Country=3&Project=3&Site=3&site_type=3&confirm_interval=8888

"""


def iniFileRead(paraFiles):
    if os.path.exists(paraFiles):  # 如果文件存在
        # 设置元组存放数据
        i = 0  # 提示用，第几个参数不完整
        parameterValue = list()
        tempf = list()
        with open(paraFiles, "r", encoding="utf-8") as f:
            for item in f.readlines():
                if "#" in item:
                    continue
                if ":" in item:
                    i = i + 1
                    tempf = item.strip().split(":")
                    if len(tempf[1]) == 0:

                        print(
                            "The "
                            + str(i)
                            + ' parameter "'
                            + (tempf[0])
                            + '" is incomplete, please check.'
                        )

                        exit()
                    else:
                        parameterValue.append(tempf[1])
        return parameterValue
    else:
        print("ini files not exist.")
        exit()


def verify_date_str_lawyer(datetime_str):
    try:
        # datetime.datetime 是不同的含义，要注意：一个是模块，一个是类
        # from datetime import datetime 如果用了这个，就只要1个dateeime了
        datetime.datetime.strptime(datetime_str, "%Y%m%d")
        return True
    except ValueError:
        return False


if __name__ == "__main__":
    while True:
        print(
            "10. YS2\n"
            "20. TW-Yilan\n"
            "30. TW-Xindian\n"
            "40. TW-Bali\n"
            "50. TW-Yongkang\n"
            "60. GVL\n"
            "70. Tpark"
        )
        ProjectId = input("Please select: ")
        # if ProjectId in ['10','20','30','40','50','60','70']:break

        if ProjectId == "10":
            iniFileName = "getdata_ys.ini"
            MainUrl = "https://hubgrade2.yansanveolia.com/Configuration/DataExport/Export?date="
            outFileNamePrefix = "ChinaHI_YS2_All_60_A_"
            break
        if ProjectId == "20":
            iniFileName = "getdata_yl.ini"
            MainUrl = "https://twyl.hubgrade.net/Configuration/DataExport/Export?date="
            outFileNamePrefix = "TWTahoo_Yilan_ALL_Operation_60_A_"
            break
        if ProjectId == "30":
            iniFileName = "getdata_xd.ini"
            MainUrl = "https://twxd.hubgrade.net/Configuration/DataExport/Export?date="
            outFileNamePrefix = "TWTahoo_Xindian_ALL_Operation_60_A_"
            break
        if ProjectId == "40":
            iniFileName = "getdata_bl.ini"
            MainUrl = "https://twbl.hubgrade.net/Configuration/DataExport/Export?date="
            outFileNamePrefix = "TWTahoo_Bali_ALL_Operation_60_A_"
            break
        if ProjectId == "50":
            iniFileName = "getdata_yk.ini"
            MainUrl = "https://twyk.hubgrade.net/Configuration/DataExport/Export?date="
            outFileNamePrefix = "TWTahoo_Yongkang_ALL_Operation_60_A_"
            break
        if ProjectId == "60":
            iniFileName = "getdata_gvl.ini"
            MainUrl = (
                "https://gvl.hubgrade.net:30443/Configuration/DataExport/Export?date="
            )
            outFileNamePrefix = "HK_GVL_ALL_Operation_60_A_"
            break
        if ProjectId == "70":
            iniFileName = "getdata_tp.ini"
            MainUrl = (
                "https://t-park.hubgrade.net/Configuration/DataExport/Export?date="
            )
            outFileNamePrefix = "HK_TPark_ALL_Operation_60_A_"
            break
        else:
            print("Error, please select again.")

    addParamater = iniFileRead(iniFileName)

    while True:
        SdateValue = input("Please input start date (YYYYMMDD) :")
        if not verify_date_str_lawyer(SdateValue):
            print("Invalid date, input again please.")
        else:
            break

    while True:
        EdateValue = input("Please input end date (YYYYMMDD) :")
        if len(EdateValue) == 0:
            EdateValue = time.strftime("%Y%m%d", time.localtime())
        if not verify_date_str_lawyer(EdateValue):
            print("Invalid date, input again please.")
        elif EdateValue < SdateValue:
            print("End date less start date, input again please.")
        else:
            break
    # print(datetime.strptime(EdateValue,'%Y%m%d').date())
    Sdate = datetime.datetime.strptime(SdateValue, "%Y%m%d").date()
    Edate = datetime.datetime.strptime(EdateValue, "%Y%m%d").date()

    for i in range((Edate - Sdate).days + 1):
        day = Sdate + datetime.timedelta(days=i)
        dayChr = day.strftime("%Y-%m-%d")
        outFileName = "data\\" + outFileNamePrefix + dayChr + ".csv"
        AllUrl = (
            MainUrl
            + dayChr
            + "&Country="
            + addParamater[0]
            + "&Project="
            + addParamater[1]
            + "&Site="
            + addParamater[2]
            + "&site_type="
            + addParamater[3]
        )

        # CookieText=addParamater[4]
        CookieText = get_cookie()

        # 接下来就是下载了，download

        herder = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7",
            "Connection": "keep-alive",
            "Cookie": CookieText,
        }

        print("downloading with requests")
        time.sleep(0.5)
        if os.path.exists(outFileName):
            os.remove(outFileName)  # 如果文件存在 删除文件If the file exists, delete

        r = requests.get(AllUrl, headers=herder)
        fp = open(outFileName, "wb")
        fp.write(r.content)
        fp.close()
        print(outFileName + " file is download.")
