from PIL import Image
from rich import print
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service

from util.get_time import write_last_run_time, last_modified_within_n_hours


def get_cookie(dev_mode=False):
    """get cookie with selenium, cookie will be write to a file and rerun within
    24 hours will only fetch existing cookie from text file instead of fetching
    a new one from website

    Args:
        dev_mode (bool, optional): if True, always fetch new cookie from
        website, run without headless and selenium browser stays open after code
        finish run. Defaults to False.

    Returns:
        [str]: cookie string within freshness of 24 hours
    """
    if last_modified_within_n_hours() and not dev_mode:
        with open("cookie.txt") as f:
            cookies_str = f.readline()
        return cookies_str

    USER_NAME = "renhua.gu"
    PASSWORD = "Password1234567890!"
    url = "https://twyk.hubgrade.net"

    options = webdriver.ChromeOptions()

    if dev_mode:
        options.add_experimental_option("detach", True)
    else:
        options.add_argument("--headless")

    s = Service(ChromeDriverManager().install())
    driver = webdriver.Chrome(service=s, options=options)
    driver.get(url)

    # fill forms
    user = driver.find_element(By.ID, "txtUserName")
    user.send_keys(USER_NAME)
    pwd = driver.find_element(By.ID, "txtpasswrd")
    pwd.send_keys(PASSWORD)

    ss_fn = "captcha_screenshot.png"
    driver.save_screenshot(ss_fn)
    # display captcha picture
    img = Image.open(ss_fn)
    img.show()
    captcha_code = input("Please enter captcha code: ")
    captcha = driver.find_element(By.ID, "J_codetext")
    captcha.send_keys(captcha_code)

    # login
    login_btn = driver.find_element(By.XPATH, "//input[@type='button']")
    login_btn.click()

    # wait till login page finish to load
    print("waiting for main page full loaded")
    WebDriverWait(driver, 30).until(
        EC.presence_of_element_located((By.ID, "divCharts"))
    )
    # cookies_list = []
    # while len(cookies_list) <= 11:

    cookies_list = driver.get_cookies()
    cookies = [f"{cookie['name']}={cookie['value']}" for cookie in driver.get_cookies()]
    cookies_str = "; ".join(cookies)

    # save cookie in case rerun within last N hours:
    with open("cookie.txt", "w") as f:
        f.write(cookies_str)

    write_last_run_time()

    return cookies_str


if __name__ == "__main__":
    output = get_cookie(True)
    print(output)
