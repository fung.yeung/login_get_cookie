from datetime import datetime

last_n_seconds = 24 * 60 * 60


def write_last_run_time():
    now = datetime.utcnow().timestamp()
    with open("last_run_time.txt", "w") as f:
        f.write(str(now))


def last_modified_within_n_hours(n=24):
    with open("last_run_time.txt") as f:
        last_modified_time = f.readline()

    last_modified_time = float(last_modified_time)
    now = datetime.utcnow().timestamp()

    time_threshold_in_sec = n * 60 * 60
    time_diff = now - last_modified_time
    return True if time_diff < time_threshold_in_sec else False


if __name__ == "__main__":
    b = last_modified_within_n_hours()
    print(b)
